#!/bin/bash

set -eux

mkdir -p /run/brimir/log /run/brimir/config /app/data/config /app/data/data

echo "Setup secrets"
if [[ ! -f /app/data/config/secrets.yml ]]; then
	cat > "/app/data/config/secrets.yml" <<END
production:
  secret_key_base: $(pwgen -1cns 30)
  google_client_id:
  google_client_secret:
END
fi

echo "Setup database"
cat > "/run/brimir/config/database.yml" <<END
production:
  adapter: postgresql
  host: ${POSTGRESQL_HOST}
  port: ${POSTGRESQL_PORT}
  database: ${POSTGRESQL_DATABASE}
  username: ${POSTGRESQL_USERNAME}
  password: ${POSTGRESQL_PASSWORD}
END

echo "Setup LDAP"
cat > "/run/brimir/config/ldap.yml" <<END
production:
  host: ${LDAP_SERVER}
  port: ${LDAP_PORT}
  attribute: username
  base: ${LDAP_USERS_BASE_DN}
  admin_user: ${LDAP_BIND_DN}
  admin_password: ${LDAP_BIND_PASSWORD}
  ssl: false
END

echo "Loading schema"
/app/code/bin/rake db:schema:load RAILS_ENV=production

echo "Changing ownership"
chown -R cloudron:cloudron /app/data /run/brimir

chmod +t /tmp

# avoid requiring nginx to serve up static files
export RAILS_SERVE_STATIC_FILES=1

/usr/local/bin/gosu cloudron:cloudron ./bin/rails server --port 8000 --binding 0.0.0.0 --environment production
