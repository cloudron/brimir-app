FROM cloudron/base:0.9.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

EXPOSE 8000

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && \
    apt-get install -y libpq-dev && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# put node run time in path for https://github.com/rails/execjs
ENV PATH /usr/local/node-4.2.6/bin:$PATH

RUN gem install bundler

RUN curl -L https://github.com/ivaldi/brimir/archive/0.7.1.tar.gz | tar -zx --strip-components 1 -f -
RUN bundle install --without sqlite mysql development test --deployment

RUN sed -i "s/<%= ENV\[\"SECRET_KEY_BASE\"\] %>/`bin/rake secret`/g" config/secrets.yml
RUN /app/code/bin/rake assets:precompile RAILS_ENV=production

RUN sed -e 's/:database_authenticatable/:ldap_authenticatable/' -i /app/code/config/application.rb

RUN rm -f /app/code/config/database.yml && ln -s /run/brimir/config/database.yml /app/code/config/database.yml
RUN rm -f /app/code/config/secrets.yml  && ln -s /app/data/config/secrets.yml /app/code/config/secrets.yml
RUN rm -f /app/code/config/ldap.yml     && ln -s /run/brimir/config/ldap.yml /app/code/config/ldap.yml
RUN rm -rf /app/code/tmp                && ln -s /tmp /app/code/tmp
RUN rm -rf /app/code/log                && ln -s /run/brimir/log /app/code/log
RUN rm -rf /app/code/data               && ln -s /app/data/data /app/code/data

ADD start.sh /app/code/

CMD [ "/app/code/start.sh" ]
